﻿using AvantMutual.Business;
using AvantMutualCodingTest.Models;
using AvantMutualCodingTest.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace AvantMutualCodingTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IDateCalculation _dateCalculation;

        public HomeController(ILogger<HomeController> logger, IDateCalculation dateCalculation)
        {
            _logger = logger;
            _dateCalculation = dateCalculation;
        }

        public IActionResult Index()
        {
            var view = new DatesViewModel { StartDate = DateTime.Today, EndDate = DateTime.Today.AddDays(1) };
            return View(view);
        }

        [HttpPost]
        public IActionResult CalculateBusinessDays( DatesViewModel input)
        {
            input.NumBusinessDays = _dateCalculation.NumberOfBusinessDays(input.StartDate, input.EndDate);

            return View(input);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
