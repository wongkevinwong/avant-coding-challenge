﻿using System;
using System.Collections.Generic;

namespace AvantMutual.Business
{
    public class DateCalculation: IDateCalculation
    {
        public bool IsBusinessDay(DateTime date)
        {
            var isBusinessDay = default(bool);
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                case DayOfWeek.Friday:
                    isBusinessDay = true;
                    break;
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    isBusinessDay = false;
                    break;
            }
            return isBusinessDay;
        }

        public bool IsTwoGivenDatesFallsWithinAWeek(DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate)
                throw new Exception("Start date must not greater than end date");

            return (endDate - startDate).Days < 7;
        }

        public int NumberOfBusinessDaysWithinAWeek(DateTime startDate, DateTime endDate, bool includesEndDate = false)
        {
            if (startDate > endDate)
                throw new Exception("Start date must not greater than end date");

            if (startDate.AddDays(6) < endDate)
                throw new Exception("start and end date must be within 7 days range");

            var numberOfDaysInBetween = Math.Max(0, includesEndDate? (endDate - startDate).Days: (endDate - startDate).Days - 1);
            var numberOfBusinessDays = 0;
            for (var i = 0; i < numberOfDaysInBetween; i++)
            {
                numberOfBusinessDays = IsBusinessDay(startDate.AddDays(i + 1)) ? ++numberOfBusinessDays : numberOfBusinessDays;
            }
            return numberOfBusinessDays;
        }


        /// <summary>
        /// Task #3
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public int NumberOfBusinessDays(DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate)
                throw new Exception("Start date must not greater than end date");

            var numberOfDaysBetween2Dates = Math.Max(0,(endDate - startDate).Days );

            var numberOfBusinessDaysFromNumberOfWeeks = 0;  
            var numberOfBusinessDaysFromRemainingOfDays = 0; 

            if (IsTwoGivenDatesFallsWithinAWeek(startDate, endDate))
            {
                numberOfBusinessDaysFromRemainingOfDays = NumberOfBusinessDaysWithinAWeek(startDate, endDate);
            } else
            {
                var numberOfWeeks = numberOfDaysBetween2Dates / 7;
                var remainingOfDays = Math.Max(0,(endDate - startDate.AddDays( (7* numberOfWeeks) -1)).Days);

                numberOfBusinessDaysFromNumberOfWeeks = (numberOfWeeks * NumberOfBusinessDaysWithinAWeek(startDate, startDate.AddDays(6), true)) + (IsBusinessDay(startDate)? (numberOfWeeks-1):0);
                numberOfBusinessDaysFromRemainingOfDays = remainingOfDays > 0 ? NumberOfBusinessDaysWithinAWeek(startDate.AddDays(7 * numberOfWeeks - 1), endDate):0;
            }
            return numberOfBusinessDaysFromNumberOfWeeks + numberOfBusinessDaysFromRemainingOfDays;


        }

        /// <summary>
        /// TASK #4
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="publicHolidays"></param>
        /// <returns></returns>
        public int NumberOfBusinessDays(DateTime startDate, DateTime endDate, params DateTime[] publicHolidays )
        {
            var numberOfBusinessDaysWithoutPublicHolidays = NumberOfBusinessDays(startDate, endDate);
            var numberOfPublicHolidayBetweenStartAndEndDate = 0;
            foreach (var publicHoliday in publicHolidays)
            {
                if (publicHoliday > startDate && publicHoliday < endDate) numberOfPublicHolidayBetweenStartAndEndDate++;
            }
            return numberOfBusinessDaysWithoutPublicHolidays - numberOfPublicHolidayBetweenStartAndEndDate;
        }

        /// <summary>
        /// TASK #5
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="sameDayPublicHolidayConsideredWeekend"></param>
        /// <param name="sameDayPublicHolidayNotConsideredWeekend"></param>
        /// <param name="certainOccurrencePublicHoliday"></param>
        /// <returns></returns>
        public int NumberOfBusinessDays(DateTime startDate, DateTime endDate, IList<DateTime> sameDayPublicHolidayConsideredWeekend, IList<DateTime> sameDayPublicHolidayNotConsideredWeekend , IList<DateTime> certainOccurrencePublicHoliday)
        {
            var numberOfBusinessDaysWithoutPublicHolidays = NumberOfBusinessDays(startDate, endDate);
            var numberOfPublicHolidayBetweenStartAndEndDate = 0;

            foreach(var holiday in sameDayPublicHolidayConsideredWeekend) //eg. anzac day (counted if it falls during weekdays), no replacement holiday if it falls on weekends
            {
                if (IsBusinessDay(holiday) && holiday > startDate && holiday < endDate) numberOfPublicHolidayBetweenStartAndEndDate += 1;
            }

            foreach (var holiday in sameDayPublicHolidayNotConsideredWeekend) //eg. new year (always counted even if it falls on weekend), it will be substitute on weekdays.
            {
                if (holiday > startDate && holiday < endDate) numberOfPublicHolidayBetweenStartAndEndDate += 1;
            }

            foreach (var holiday in certainOccurrencePublicHoliday) //eg. queen birthday, basically same as new year (always counted even if it falls on weekend), it will be substitute on weekdays.
            {
                if (holiday > startDate && holiday < endDate) numberOfPublicHolidayBetweenStartAndEndDate += 1;
            }

            return numberOfBusinessDaysWithoutPublicHolidays - numberOfPublicHolidayBetweenStartAndEndDate;
        }
    }
}
