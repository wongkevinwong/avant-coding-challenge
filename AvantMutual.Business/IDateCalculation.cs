﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AvantMutual.Business
{
    public interface IDateCalculation
    {
        bool IsBusinessDay(DateTime date);
        bool IsTwoGivenDatesFallsWithinAWeek(DateTime startDate, DateTime endDate);
        int NumberOfBusinessDaysWithinAWeek(DateTime startDate, DateTime endDate, bool includesEndDate = false);
        int NumberOfBusinessDays(DateTime startDate, DateTime endDate);
        int NumberOfBusinessDays(DateTime startDate, DateTime endDate, params DateTime[] publicHolidays);
        int NumberOfBusinessDays(DateTime startDate, DateTime endDate, IList<DateTime> sameDayPublicHolidayConsideredWeekend, IList<DateTime> sameDayPublicHolidayNotConsideredWeekend, IList<DateTime> certainOccurrencePublicHoliday);

    }
}
