using Microsoft.VisualStudio.TestTools.UnitTesting;
using AvantMutual.Business;
using System;

namespace AvantMutual.Business.Test
{
    [TestClass]
    public class DateCalculationTest
    {
        [TestMethod]
        public void GivenStartDateAndEndDate_whenBothDatesFallWithinAWeek_MustReturnTrue()
        {
            var startDate = DateTime.Parse( "17 Jan 2021");
            var endDate = DateTime.Parse("23 Jan 2021");

            var dateCalculation = new DateCalculation();
            var actual = dateCalculation.IsTwoGivenDatesFallsWithinAWeek(startDate, endDate);

            Assert.AreEqual(true, actual);
        }

        [TestMethod]
        public void GivenStartDateAndEndDate_whenBothDatesDoNotFallWithinAWeek_MustReturnFalse()
        {
            var startDate = DateTime.Parse("17 Jan 2021");
            var endDate = DateTime.Parse("24 Jan 2021");

            var dateCalculation = new DateCalculation();
            var actual = dateCalculation.IsTwoGivenDatesFallsWithinAWeek(startDate, endDate);

            Assert.AreEqual(false, actual);
        }

        [TestMethod]
        public void GivenStartDateAndEndDate_whenBothDatesAreBusinessDaysFallWithinAWeek_MustReturnCorrectResult()
        {
            var startDate = DateTime.Parse("17 Jan 2021");
            var endDate = DateTime.Parse("18 Jan 2021");

            var dateCalculation = new DateCalculation();
            var actual = dateCalculation.NumberOfBusinessDaysWithinAWeek(startDate, endDate);

            Assert.AreEqual(0, actual);


            startDate = DateTime.Parse("17 Jan 2021");
            endDate = DateTime.Parse("19 Jan 2021");

            actual = dateCalculation.NumberOfBusinessDaysWithinAWeek(startDate, endDate);

            Assert.AreEqual(1, actual);

            startDate = DateTime.Parse("17 Jan 2021");
            endDate = DateTime.Parse("23 Jan 2021");

            actual = dateCalculation.NumberOfBusinessDaysWithinAWeek(startDate, endDate);

            Assert.AreEqual(5, actual);

            startDate = DateTime.Parse("7 Aug 2014");
            endDate = DateTime.Parse("11 Aug 2014");

            actual = dateCalculation.NumberOfBusinessDaysWithinAWeek(startDate, endDate);

            Assert.AreEqual(1, actual);

            startDate = DateTime.Parse("13 Aug 2014");
            endDate = DateTime.Parse("21 Aug 2014");

            Assert.ThrowsException<Exception>(() => dateCalculation.NumberOfBusinessDaysWithinAWeek(startDate, endDate));
        }

        [TestMethod]
        public void GivenStartDateAndEndDate_whenBothDatesAreBusinessDays_MustReturnCorrectResult()
        {
            var startDate = DateTime.Parse("13 Aug 2014");
            var endDate = DateTime.Parse("21 Aug 2014");
            var dateCalculation = new DateCalculation();
            var numberOfBusinessDays = dateCalculation.NumberOfBusinessDays(startDate, endDate);

            Assert.AreEqual(5, numberOfBusinessDays);

            startDate = DateTime.Parse("13 Aug 2014");
            endDate = DateTime.Parse("20 Aug 2014");
            numberOfBusinessDays = dateCalculation.NumberOfBusinessDays(startDate, endDate);

            Assert.AreEqual(4, numberOfBusinessDays);

            startDate = DateTime.Parse("13 Aug 2014");
            endDate = DateTime.Parse("19 Aug 2014");
            numberOfBusinessDays = dateCalculation.NumberOfBusinessDays(startDate, endDate);

            Assert.AreEqual(3, numberOfBusinessDays);

            startDate = DateTime.Parse("13 Aug 2014");
            endDate = DateTime.Parse("28 Aug 2014");
            numberOfBusinessDays = dateCalculation.NumberOfBusinessDays(startDate, endDate);

            Assert.AreEqual(10, numberOfBusinessDays);

            startDate = DateTime.Parse("13 Aug 2014");
            endDate = DateTime.Parse("4 Sep 2014");
            numberOfBusinessDays = dateCalculation.NumberOfBusinessDays(startDate, endDate);

            Assert.AreEqual(15, numberOfBusinessDays);

            startDate = DateTime.Parse("2 Jan 2021");
            endDate = DateTime.Parse("18 Jan 2021");
            numberOfBusinessDays = dateCalculation.NumberOfBusinessDays(startDate, endDate);

            Assert.AreEqual(10, numberOfBusinessDays);

            startDate = DateTime.Parse("2 Jan 2021");
            endDate = DateTime.Parse("19 Jan 2021");
            numberOfBusinessDays = dateCalculation.NumberOfBusinessDays(startDate, endDate);

            Assert.AreEqual(11, numberOfBusinessDays);
        }

        [TestMethod]
        public void GivenStartDateAndEndDate_whenBothDatesAreBusinessDaysAndPublicHolidayInBetween_MustReturnCorrectResult()
        {
            var startDate = DateTime.Parse("13 Aug 2014");
            var endDate = DateTime.Parse("21 Aug 2014");
            var dateCalculation = new DateCalculation();
            var numberOfBusinessDays = dateCalculation.NumberOfBusinessDays(startDate, endDate, DateTime.Parse("17 Jul 2014"), DateTime.Parse("6 Aug 2014"), DateTime.Parse("15 Aug 2014"));

            Assert.AreEqual(4, numberOfBusinessDays);

           
        }
    }
}
